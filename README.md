###### Se controla la rotación de pantalla (no se paran los hilos al girar y los items están dentro de un scroll) y que haya conexión a internet.
###### Para la conexión a internet se ha utilizado la clase RestClient.
###### Para la creación del JSON se ha utilizado GSON.
###### Se controla la extensión del fichero, la cuál se añade automáticamente (o se remplaza por la otra extensión)
###### La extensión también cambia al cambiar el valor del radiobutton
###### Al pulsar el botón de calcular, comprueba si ya hay un hilo corriendo. Si lo hay, no hace nada. Si no lo hay, empieza él el hilo
###### El programa descarga de https://www.quinielista.es/xml/temporada.asp las jornadas, busca la última completada, y la convierte en un objeto tipo Jornada. Después, en función del radiobutton seleccionado, convierte esa jornada en XML o JSON y la sube al servidor con el nombre pasado. Si todo está correcto, descarga el fichero de apuestas pasado. Si no ocurre ningún fallo, las compara y las sube al servidor con el nombre pasado.