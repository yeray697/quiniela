package com.yeray697.quiniela;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by yeray697 on 15/01/17.
 */

public class Jornada {
    @SerializedName("jornada")
    private int jornada;
    @SerializedName("partidos")
    private ArrayList<Partido> partidos;
    @SerializedName("recaudacion")
    private double recaudacion;
    @SerializedName("el15")
    private double el15;
    @SerializedName("el14")
    private double el14;
    @SerializedName("el13")
    private double el13;
    @SerializedName("el12")
    private double el12;
    @SerializedName("el11")
    private double el11;
    @SerializedName("el10")
    private double el10;
    @SerializedName("apuesta")
    private double apuesta;

    private int ganador10,ganador11,ganador12,ganador13,ganador14,ganador15;

    public Jornada() {
    }

    public Jornada(int jornada, ArrayList<Partido> partidos, double recaudacion, double el15, double el14, double el13, double el12, double el11, double el10, double apuesta) {
        this.jornada = jornada;
        this.recaudacion = recaudacion;
        /*
        this.recaudacion = recaudacion / 100;this.el15 = el15 / 100;
        this.el14 = el14 / 100;
        this.el13 = el13 / 100;
        this.el12 = el12 / 100;
        this.el11 = el11 / 100;
        this.el10 = el10 / 100;
        this.apuesta = apuesta / 10000;*/
        this.el15 = el15;
        this.el14 = el14;
        this.el13 = el13;
        this.el12 = el12;
        this.el11 = el11;
        this.el10 = el10;
        this.apuesta = apuesta;
        this.partidos = new ArrayList<>();
        for (Partido partido : partidos) {
            this.partidos.add(new Partido(partido.getPartido(),partido.getResultado()));
        }
        this.ganador10 = 0;
        this.ganador11 = 0;
        this.ganador12 = 0;
        this.ganador13 = 0;
        this.ganador14 = 0;
        this.ganador15 = 0;
    }

    public int getJornada() {
        return jornada;
    }

    public void setJornada(int jornada) {
        this.jornada = jornada;
    }

    public ArrayList<Partido> getPartidos() {
        return partidos;
    }

    public void addPartido(Partido partido) {
        if (this.partidos == null)
            this.partidos = new ArrayList<>();
        this.partidos.add(partido);
    }

    public double getRecaudacion() {
        return recaudacion;
    }

    public void setRecaudacion(double recaudacion) {
        this.recaudacion = Utils.round(recaudacion,2);
    }

    public double getEl15() {
        return el15;
    }

    public void setEl15(double el15) {
        this.el15 = Utils.round(el15,2);
    }

    public double getEl14() {
        return el14;
    }

    public void setEl14(double el14) {
        this.el14 = Utils.round(el14,2);
    }

    public double getEl13() {
        return el13;
    }

    public void setEl13(double el13) {
        this.el13 = Utils.round(el13,2);
    }

    public double getEl12() {
        return el12;
    }

    public void setEl12(double el12) {
        this.el12 = Utils.round(el12,2);
    }

    public double getEl11() {
        return el11;
    }

    public void setEl11(double el11) {
        this.el11 = Utils.round(el11,2);
    }

    public double getEl10() {
        return el10;
    }

    public void setEl10(double el10) {
        this.el10 = Utils.round(el10,2);
    }

    public double getApuesta() {
        return apuesta;
    }

    public void setApuesta(double apuesta) {
        this.apuesta = Utils.round(apuesta,2);
    }

    public boolean jornadaFinalizada() {
        return el10 == 0 && el11 == 0 && el12 == 0 && el13 == 0 && el14 == 0 && el15 == 0;
    }

    public static Jornada clone(Jornada jornada) {
        return new Jornada(jornada.getJornada(),jornada.getPartidos(),
                jornada.getRecaudacion(),jornada.getEl15(),
                jornada.getEl14(),jornada.getEl13(),jornada.getEl12(),
                jornada.getEl11(),jornada.getEl10(),jornada.getApuesta());
    }

    public int comprobarApuesta(String apuesta) {
        int resultado = 0;
        String aux;
        for (int i = 0; i < apuesta.length(); i++) {

            if (resultado + (apuesta.length() - 2 - i) < 10 || i > apuesta.length() - 2)
                break;
            if (i < apuesta.length() - 2) {
                aux = String.valueOf(apuesta.charAt(i));
            } else {
                if (resultado == 14)
                    aux = (String.valueOf(apuesta.charAt(i)) + String.valueOf(apuesta.charAt(i + 1)));
                else
                    aux = "";
            }
            try {

                if (String.valueOf(apuesta.charAt(i)).equals(partidos.get(i).getResultado()))
                    resultado++;
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
        return resultado;
    }

    public void addGanador10() {
        ganador10 ++;

    }

    public void addGanador11() {
        ganador11 ++;
    }

    public void addGanador12() {
        ganador12 ++;
    }

    public void addGanador13() {
        ganador13 ++;
    }

    public void addGanador14() {
        ganador14 ++;
    }

    public void addGanador15() {
        ganador15 ++;
    }

    public int getGanador10() {
        return ganador10;
    }

    public int getGanador11() {
        return ganador11;
    }

    public int getGanador12() {
        return ganador12;
    }

    public int getGanador13() {
        return ganador13;
    }

    public int getGanador14() {
        return ganador14;
    }

    public int getGanador15() {
        return ganador15;
    }
}
