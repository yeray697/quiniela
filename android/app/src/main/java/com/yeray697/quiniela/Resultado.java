package com.yeray697.quiniela;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yeray697 on 15/01/17.
 */

public class Resultado {
    @SerializedName("jornada")
    private int jornada;
    @SerializedName("nJugadores")
    private int nJugadores;
    @SerializedName("recaudado")
    private double recaudado;
    @SerializedName("premioTotal")
    private double premioTotal;
    @SerializedName("premioDe10")
    private double premio10;
    @SerializedName("ganadoresDe10")
    private int ganadores10;
    @SerializedName("premioDe11")
    private double premio11;
    @SerializedName("ganadoresDe11")
    private int ganadores11;
    @SerializedName("premioDe12")
    private double premio12;
    @SerializedName("ganadoresDe12")
    private int ganadores12;
    @SerializedName("premioDe13")
    private double premio13;
    @SerializedName("ganadoresDe13")
    private int ganadores13;
    @SerializedName("premioDe14")
    private double premio14;
    @SerializedName("ganadoresDe14")
    private int ganadores14;
    @SerializedName("premioDe15")
    private double premio15;
    @SerializedName("ganadoresDe15")
    private int ganadores15;

    public Resultado(int jornada, int nJugadores, double recaudado, int ganadores10, int ganadores11, int ganadores12, int ganadores13, int ganadores14, int ganadores15,
                     double premio10,double premio11,double premio12,double premio13,double premio14,double premio15) {
        this.jornada = jornada;
        this.nJugadores = nJugadores;
        this.recaudado = recaudado;
        this.premio10 = premio10;
        this.ganadores10 = ganadores10;
        this.premio11 = premio11;
        this.ganadores11 = ganadores11;
        this.premio12 = premio12;
        this.ganadores12 = ganadores12;
        this.premio13 = premio13;
        this.ganadores13 = ganadores13;
        this.premio14 = premio14;
        this.ganadores14 = ganadores14;
        this.premio15 = premio15;
        this.ganadores15 = ganadores15;
        this.premioTotal = Utils.round(this.premio10 + this.premio11 + this.premio12 + this.premio13 + this.premio14 + this.premio15, 2);
    }

    public int getJornada() {
        return jornada;
    }

    public int getnJugadores() {
        return nJugadores;
    }

    public double getRecaudado() {
        return recaudado;
    }

    public int getGanadores10() {
        return ganadores10;
    }

    public int getGanadores11() {
        return ganadores11;
    }

    public int getGanadores12() {
        return ganadores12;
    }

    public int getGanadores13() {
        return ganadores13;
    }

    public int getGanadores14() {
        return ganadores14;
    }

    public int getGanadores15() {
        return ganadores15;
    }

    public double getPremio10() {
        return premio10;
    }

    public double getPremio11() {
        return premio11;
    }

    public double getPremio12() {
        return premio12;
    }

    public double getPremio13() {
        return premio13;
    }

    public double getPremio14() {
        return premio14;
    }

    public double getPremio15() {
        return premio15;
    }

    public double getPremioTotal() {
        return premioTotal;
    }
}
