package com.yeray697.quiniela;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yeray697 on 15/01/17.
 */
public class Partido {
    @SerializedName("partido")
    private int partido;
    @SerializedName("resultado")
    private String resultado;

    public Partido(int partido, String resultado) {
        this.partido = partido;
        this.resultado = resultado;
    }
    public Partido(){

    }

    public int getPartido() {
        return partido;
    }

    public void setPartido(int partido) {
        this.partido = partido;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
}
