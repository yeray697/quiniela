package com.yeray697.quiniela;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Xml;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;

/**
 * Created by yeray697 on 15/01/17.
 */

public class Utils {
    private final static String QUINIELA = "quiniela";
    private final static String PARTIT = "partit";
    private final static String JORNADA = "jornada";
    private final static String RECAUDACION = "recaudacion";
    private final static String EL_15 = "el15";
    private final static String EL_14 = "el14";
    private final static String EL_13 = "el13";
    private final static String EL_12 = "el12";
    private final static String EL_11 = "el11";
    private final static String EL_10 = "el10";
    private final static String APUESTA = "apuesta";
    private final static String NUM = "num";
    private final static String SIG = "sig";

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static Jornada readXML(String xml) throws XmlPullParserException {
        Jornada jornada = null;
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput( new StringReader(xml));
        int eventType = xpp.getEventType();
        String atributo;
        Partido partido;
        Jornada ultimaJornadaFinalizada = null;
        boolean fin = false;
        while (eventType != XmlPullParser.END_DOCUMENT && !fin){
            try {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        name = xpp.getName();
                        if (name.equals(QUINIELA)) {
                            jornada = new Jornada();
                            for (int i = 0; i < xpp.getAttributeCount(); i++) {
                                atributo = xpp.getAttributeValue(i);
                                switch (xpp.getAttributeName(i)) {
                                    case JORNADA:
                                        jornada.setJornada(Integer.parseInt(atributo));
                                        break;
                                    case RECAUDACION:
                                        jornada.setRecaudacion(Double.parseDouble(atributo)/100);
                                        break;
                                    case EL_15:
                                        jornada.setEl15(Double.parseDouble(atributo)/100);
                                        break;
                                    case EL_14:
                                        jornada.setEl14(Double.parseDouble(atributo)/100);
                                        break;
                                    case EL_13:
                                        jornada.setEl13(Double.parseDouble(atributo)/100);
                                        break;
                                    case EL_12:
                                        jornada.setEl12(Double.parseDouble(atributo)/100);
                                        break;
                                    case EL_11:
                                        jornada.setEl11(Double.parseDouble(atributo)/100);
                                        break;
                                    case EL_10:
                                        jornada.setEl10(Double.parseDouble(atributo)/100);
                                        break;
                                    case APUESTA:
                                        jornada.setApuesta(Double.parseDouble(atributo)/10000);
                                        break;
                                }
                            }
                        } else if (name.equals(PARTIT)) {
                            partido = new Partido();
                            for (int i = 0; i < xpp.getAttributeCount(); i++) {
                                atributo = xpp.getAttributeValue(i);
                                switch (xpp.getAttributeName(i)) {
                                    case NUM:
                                            partido.setPartido(Integer.parseInt(atributo));
                                        break;
                                    case SIG:
                                        partido.setResultado(atributo);
                                        break;
                                }
                            }
                            jornada.addPartido(partido);
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = xpp.getName();
                        if (name.equals(QUINIELA)) {
                            if (jornada.jornadaFinalizada()) {
                                fin = true;
                            } else {
                                ultimaJornadaFinalizada = Jornada.clone(jornada);
                            }
                        }
                }
                eventType = xpp.next();
            }
            catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
            }
        }

        return ultimaJornadaFinalizada;
    }

    public static File jornadaToXml(Jornada jornada, String fichero) throws IOException {
        FileOutputStream fout;
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero);
        fout = new FileOutputStream(miFichero);
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(fout, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true); //poner tabulación
        serializer.startTag(null, "jornada");
        addTagXML(serializer,"jornada",jornada.getJornada());
        addTagXML(serializer,"apuesta",jornada.getApuesta());
        addTagXML(serializer,"el15",jornada.getEl15());
        addTagXML(serializer,"el14",jornada.getEl14());
        addTagXML(serializer,"el13",jornada.getEl13());
        addTagXML(serializer,"el12",jornada.getEl12());
        addTagXML(serializer,"el11",jornada.getEl11());
        addTagXML(serializer,"el10",jornada.getEl10());
        addTagXML(serializer,"recaudacion",jornada.getRecaudacion());
        serializer.startTag(null,"partidos");
        for (Partido partido: jornada.getPartidos()) {
            serializer.startTag(null,"partido");
            serializer.attribute(null,"numero", String.valueOf(partido.getPartido()));
            serializer.attribute(null,"resultado", String.valueOf(partido.getResultado()));
            serializer.endTag(null,"partido");
        }
        serializer.endTag(null,"partidos");
        serializer.endTag(null, "jornada");
        serializer.endDocument();
        serializer.flush();
        fout.close();
        return miFichero;
    }

    private static void addTagXML(XmlSerializer serializer, String name, int value) throws IOException {
        serializer.startTag(null, name);
        serializer.text(String.valueOf(value));
        serializer.endTag(null, name);
    }
    private static void addTagXML(XmlSerializer serializer, String name, double value) throws IOException {
        serializer.startTag(null, name);
        serializer.text(String.valueOf(value));
        serializer.endTag(null, name);
    }

    public static File jornadaToJSON(Jornada jornada, String fichero) throws IOException {
        OutputStreamWriter out;
        File miFichero;
        String texto;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        texto = gson.toJson(jornada);
        miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero);
        out = new FileWriter(miFichero);
        out.write(texto);
        out.close();
        return miFichero;
    }

    public static File resultadosToXml(Resultado resultado, String fichero) throws IOException {FileOutputStream fout;
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero);
        fout = new FileOutputStream(miFichero);
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(fout, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true); //poner tabulación
        serializer.startTag(null, "resultados");
        addTagXML(serializer,"jornada",resultado.getJornada());
        addTagXML(serializer,"nJugadores",resultado.getnJugadores());
        addTagXML(serializer,"recaudado",resultado.getRecaudado());
        addTagXML(serializer,"premioTotal",resultado.getPremioTotal());
        addTagXML(serializer,"premioDe10",resultado.getPremio10());
        addTagXML(serializer,"ganadoresDe10",resultado.getGanadores10());
        addTagXML(serializer,"premioDe11",resultado.getPremio11());
        addTagXML(serializer,"ganadoresDe11",resultado.getGanadores11());
        addTagXML(serializer,"premioDe12",resultado.getPremio12());
        addTagXML(serializer,"ganadoresDe12",resultado.getGanadores12());
        addTagXML(serializer,"premioDe13",resultado.getPremio13());
        addTagXML(serializer,"ganadoresDe13",resultado.getGanadores13());
        addTagXML(serializer,"premioDe14",resultado.getPremio14());
        addTagXML(serializer,"ganadoresDe14",resultado.getGanadores14());
        addTagXML(serializer,"premioDe15",resultado.getPremio15());
        addTagXML(serializer,"ganadoresDe15",resultado.getGanadores15());
        serializer.endTag(null, "resultados");
        serializer.endDocument();
        serializer.flush();
        fout.close();
        return miFichero;
    }

    public static File resultadosToJSON(Resultado resultado, String fichero) throws IOException {
        OutputStreamWriter out;
        File miFichero;
        String texto;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        texto = gson.toJson(resultado);
        miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero);
        out = new FileWriter(miFichero);
        out.write(texto);
        out.close();
        return miFichero;
    }
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
}
