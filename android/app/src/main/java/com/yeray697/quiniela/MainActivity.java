package com.yeray697.quiniela;

import android.app.ProgressDialog;
import android.os.Environment;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class MainActivity extends AppCompatActivity {

    private static final String UPLOAD_WEB = "http://alumno.club/superior/yeray/upload.php";
    private static final String WEB_RESULTADOS = "http://www.quinielista.es/xml/temporada.asp";
    private String resultados,apuestas,aciertos;
    private Jornada jornada;
    private Resultado resultado;
    RadioButton rbXml, rbJson;
    EditText etResultados, etApuestas, etAciertos;
    Button btCalcular;

    boolean ejecutandose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ejecutandose = false;
        rbXml = (RadioButton) findViewById(R.id.rbXml);
        rbJson = (RadioButton) findViewById(R.id.rbJson);
        etResultados = (EditText) findViewById(R.id.etResultados);
        etApuestas = (EditText) findViewById(R.id.etApuestas);
        etAciertos = (EditText) findViewById(R.id.etAciertos);
        btCalcular = (Button) findViewById(R.id.btCalcular);
        btCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ejecutandose) {
                    ejecutandose = true;
                    if (Utils.isNetworkAvailable(MainActivity.this))
                        calcular();
                    else {
                        Toast.makeText(MainActivity.this, "No tienes conexión a internet", Toast.LENGTH_SHORT).show();
                        ejecutandose = false;
                    }
                }
            }
        });
        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                aciertos = etAciertos.getText().toString();
                resultados = etResultados.getText().toString();
                if (b) {
                    if (aciertos.endsWith(".json"))
                        aciertos = aciertos.replace(".json",".xml");
                    if (!aciertos.endsWith(".xml"))
                        aciertos += ".xml";

                    if (resultados.endsWith(".json"))
                        resultados = resultados.replace(".json",".xml");
                    if (!resultados.endsWith(".xml"))
                        resultados += ".xml";
                } else {
                    if (aciertos.endsWith(".xml"))
                        aciertos = aciertos.replace(".xml",".json");
                    if (!aciertos.endsWith(".json"))
                        aciertos += ".json";

                    if (resultados.endsWith(".xml"))
                        resultados = resultados.replace(".xml",".json");
                    if (!resultados.endsWith(".json"))
                        resultados += ".json";
                }
                if (!aciertos.equals(etAciertos.getText().toString())){
                    etAciertos.setText(aciertos);
                }
                if (!resultados.equals(etResultados.getText().toString())){
                    etResultados.setText(resultados);
                }
            }
        };
        rbXml.setOnCheckedChangeListener(listener);
        etAciertos.setHint("Aciertos y premios");
    }

    private void calcular() {
        boolean error = false;
        resultados = etResultados.getText().toString();
        apuestas = etApuestas.getText().toString();
        aciertos = etAciertos.getText().toString();
        if (TextUtils.isEmpty(resultados)) {
            etResultados.setError("El campo no puede estar vacío");
            error = true;
        }
        if (TextUtils.isEmpty(apuestas)) {
            etApuestas.setError("El campo no puede estar vacío");
            error = true;
        }
        if (TextUtils.isEmpty(aciertos)) {
            etAciertos.setError("El campo no puede estar vacío");
            error = true;
        }
        if (!error) {

            final ProgressDialog progreso = new ProgressDialog(this);
            RestClient.get(WEB_RESULTADOS, new AsyncHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Descargando resultados . . .");
                    progreso.setCancelable(false);
                    progreso.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progreso.dismiss();
                    String xml = new String(responseBody);
                    try {
                       jornada = Utils.readXML(xml);
                        File file = null;
                        if (rbXml.isChecked()){
                            if (resultados.endsWith(".json"))
                                resultados = aciertos.replace(".json",".xml");
                            if (!resultados.endsWith(".xml"))
                                resultados += ".xml";
                            file = Utils.jornadaToXml(jornada,resultados);
                        } else {
                            if (resultados.endsWith(".xml"))
                                resultados = aciertos.replace(".xml",".json");
                            if (!resultados.endsWith(".json"))
                                resultados += ".json";
                            file = Utils.jornadaToJSON(jornada,resultados);
                        }
                        subirResultados(file);
                    } catch (XmlPullParserException | IOException e) {
                        e.printStackTrace();
                        ejecutandose = false;
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progreso.dismiss();
                    Toast.makeText(MainActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    ejecutandose = false;
                }
            });

        } else
            ejecutandose = false;
    }

    private void subirResultados(final File fichero) throws IOException {
        final ProgressDialog progreso = new ProgressDialog(this);
        RequestParams params = new RequestParams();
        params.put("uploadedfile", fichero);
        RestClient.post(UPLOAD_WEB, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Subiendo resultados . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progreso.dismiss();
                //fichero.delete();
                Toast.makeText(MainActivity.this, "Fichero 'resultados' subido al servidor", Toast.LENGTH_SHORT).show();
                descargarApuestas();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progreso.dismiss();
                //fichero.delete();
                Toast.makeText(MainActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                ejecutandose = false;

            }
        });
    }

    private void descargarApuestas() {
        final ProgressDialog progreso = new ProgressDialog(this);
        RestClient.get(apuestas, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Descargando apuestas . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progreso.dismiss();
                Toast.makeText(MainActivity.this, "Apuestas descargadas", Toast.LENGTH_SHORT).show();
                String apuestas = new String (responseBody);
                comprobarApuestas(apuestas.split("\n"));
                try {
                    File file = null;
                    if (rbXml.isChecked()){
                        if (aciertos.endsWith(".json"))
                            aciertos = aciertos.replace(".json",".xml");
                        if (!aciertos.endsWith(".xml"))
                            aciertos += ".xml";
                        file = Utils.resultadosToXml(resultado,aciertos);
                    } else {
                        if (aciertos.endsWith(".xml"))
                            aciertos = aciertos.replace(".xml",".json");
                        if (!aciertos.endsWith(".json"))
                            aciertos += ".json";
                        file = Utils.resultadosToJSON(resultado,aciertos);
                    }
                    subirPremios(file);
                } catch (IOException e) {
                    e.printStackTrace();
                    ejecutandose = false;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progreso.dismiss();
                Toast.makeText(MainActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                ejecutandose = false;
            }
        });
    }

    private void subirPremios(final File fichero) throws FileNotFoundException {
        RequestParams params = new RequestParams();
        params.put("uploadedfile", fichero);
        final ProgressDialog progreso = new ProgressDialog(this);
        RestClient.post(UPLOAD_WEB,params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Subiendo premios . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String web = new String(responseBody);
                progreso.dismiss();
                Toast.makeText(MainActivity.this, "Premios subidos", Toast.LENGTH_SHORT).show();
                //fichero.delete();
                ejecutandose = false;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progreso.dismiss();
                Toast.makeText(MainActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                //fichero.delete();
                ejecutandose = false;
            }
        });
    }

    private void comprobarApuestas(String[] apuestas) {
        int aciertos;
        for (String apuesta:apuestas) {
            apuesta = apuesta.replace("\r","");
            aciertos = jornada.comprobarApuesta(apuesta);
            switch (aciertos){
                case 10:
                    jornada.addGanador10();
                    break;
                case 11:
                    jornada.addGanador11();
                    break;
                case 12:
                    jornada.addGanador12();
                    break;
                case 13:
                    jornada.addGanador13();
                    break;
                case 14:
                    jornada.addGanador14();
                    break;
                case 15:
                    jornada.addGanador15();
                    break;
            }
        }
        resultado = new Resultado(jornada.getJornada(),apuestas.length,jornada.getRecaudacion(),jornada.getGanador10(),
                jornada.getGanador11(),jornada.getGanador12(),jornada.getGanador13(),
                jornada.getGanador14(),jornada.getGanador15(),jornada.getEl10(),jornada.getEl11(),
                jornada.getEl12(),jornada.getEl13(),jornada.getEl14(),jornada.getEl15());
    }
}
